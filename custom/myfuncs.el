;;; myfuncs.el --- My function scripts
;;; Commentary:
;;;
;;; Code:
(setq helm-boring-buffer-regexp-list '("\\` " "\\*helm" "\\*helm-mode" "\\*Echo Area" "\\*Minibuf"))
;; (setenv "GTAGSDBPATH" "/home/santa/.gtags/" )

(defun initproject()
  "Init my version project: git init, .gitignore with gtags file and CMake"
  (interactive)
  (if (eq nil (file-exists-p ".git"))
      (initgit)
    )
  )

(defun initgit()
  "Initi .git"
  (shell-command "git init")
  (append-to-file "GTAGS\nGPATH\nGRTAGS\nbuild/\n" nil ".gitignore")
  (shell-command "gtags -c")
  )

(defun createlocal-dir()
  "Create .dir-locals.el in root dir"
  (interactive)
  (append-to-file "(( nil . ((company-clang-arguments . (\"-I\")))))" nil ".dir-locals.el")
  )

(defun lteFunc(number-search)
  (interactive "nNumber: ")
  (message (number-to-string number-search))
  (save-excursion
    (search-forward-regexp (concat "^" (number-to-string number-search)))
    (backward-char 1)
    (if (equal number-search 7)
        (setq myurl "wget \"http://10.77.93.232:3000/lte/add?msisdn=")
      (setq myurl "wget \" ttp://10.77.93.232:3000/lte/add?msisdn=7"))
    (insert myurl)
    ))

(setq enable-local-variables t)
(provide 'myfuncs)

;;; myfuncs.el ends here
;;; Local Variables:
;;; byte-compile-warnings: (not free-vars)
;;; End:
