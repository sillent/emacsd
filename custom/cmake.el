;;; cmake.el --- Setup cmake
;;; Commentary:
;;; Code:
(require 'cmake-mode)

(provide 'cmake)
;;; cmake.el ends here
