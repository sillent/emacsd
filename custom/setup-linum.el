;;; setup-linum.el --- linum mode
;;; Commentary:
;;; Code:
(require 'linum+)
(setq linum-format "%d ")
(global-linum-mode 1)

(provide 'setup-linum)
;;; setup-linum.el ends here
