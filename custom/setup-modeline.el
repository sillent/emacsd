;;; setup-modeline.el --- Setup modeline
;;; Commentary:
;;; Code:
(setq sml/theme 'dark)
(sml/setup)

(provide 'setup-modeline)
;;; setup-modeline.el ends here
